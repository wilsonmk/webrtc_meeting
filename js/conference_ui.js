var conference_ui = function(ui) {
    var that = this;
    var login_user = ui.login_para.loginUser;
    var login_role = ui.login_para.loginRole;
    var login_mail = ui.login_para.loginMail;
    var login_conftoken = ui.login_para.loginconfToken;
    var userlist_div = ui.div_list.userlist_div;
    var videoshow_div = ui.div_list.videoshow_div;
    var fileshow_div = ui.div_list.fileshow_div;
    var whiteboard_div = ui.div_list.whiteboard_div;
    var textchat_div = ui.div_list.textchat_div;
    var textctrl_div = ui.div_list.textctrl_div;
    var commondialog_div = ui.div_list.commondialog_div;
    var commondialog_tr = ui.div_list.commondialog_tr;
    var userlist_tr = ui.div_list.userlist_tr;

    var socketio_loadtimer = null;
    var socketio_loaded = false;
    var conferenceLoaded = false;
    var fsLoaded = false;
    var basicSignalingSocket = { };
    var conferenceMgr = null;
    var isBasicSignalOnboard = false;
    var haveGroupChat = false;
    //var numVideoOBJS = 15;
    //var fileListDiv = new FileListDiv(); 
    var fileListDiv = null;
    var conference_fs  = null;
    var maxParticipants = 15;
    var participant_list = [maxParticipants + 1];
    var fs_url = "192.168.1.95:8890";

    input = document.querySelector('input[type=text]');
    output = document.querySelector('#output');

    uniqueToken = function() {
        var s4 = function () {
            return Math.floor(Math.random() * 0x10000).toString(16);
        };
        return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
    };

    //隐藏展开列表
    $("#aFloatTools_Hide").live("click", function () {
        fileListDiv.hideList();
    });
    $("#aFloatTools_Show").live("click", function () {
        //加载中
        fileListDiv.showSyncState("加载列表中...");
        //动画展开
        fileListDiv.showList();
        //同步列表
        fileListDiv.syncList(conference_fs, function (file) {
            if (typeof(file.url) == "undefined") {
                downloadFile(file.roomId, file.fileName);
            }
            $("#fileViewer").attr("src", file.url);

        });
        //Here add your statement to synchronize with other peer
        // You can add signaling statement ,Jack

        //
    });
    //this.self = {
    //    userToken: uniqueToken()
    //};
    var current_user = {
        userToken:  uniqueToken()
    };

    this.ezDialog = null;

    this.refreshFilelistDiv = function(roomId, fileName, url,callbackFunc){
        fileListDiv.modifyUrl(roomId, fileName, url);
        fileListDiv.updateList(function (file) {
            if (typeof(file.url) == "undefined") {
                callbackFunc && callbackFunc(file.roomId, file.fileName);
            }
        });
    };

    this.getConferenceFS = function() {
        //return conferenceMgr ? conferenceMgr.getConferenceFS() : null;
        return conference_fs;
    };
    this.getCurrentUser = function() {
        return current_user;
    };
    this.setConfToken = function(conf_token) {
        current_user.confToken = conf_token;
    };
    this.getConferenceFilelist = function() {
        return conferenceMgr ? conferenceMgr.getConferenceFilelist() : null;
    };
    this.getCurrentFileName = function() {
        return conferenceMgr ? conferenceMgr.getCurrentFileName() : null;
    }
    this.getBasicConfSignal = function() {
        return basicSignalingSocket;
    };
    this.getConferenceSignalingSock = function() {
       return conferenceMgr ? conferenceMgr.getConferenceSignalingSock() : null;
    };
    this.InviteConferenceWhiteboard = function(fileName){
        conferenceMgr && conferenceMgr.onConferenceWhiteboard(fileName);
    };

    this.getUserNumber = function(){
        return userlist_div.childNodes.length;
    };

    this.getLocalvideo = function() {
        return userlist_div.childNodes[1].getAttribute("id");
    };

    this.setCaller = function(call_list) {
        for (i = 0; i < maxParticipants-1; i++) {
        //for (i = 1; i < maxParticipants-1; i++) {

            //caller_list[i] = userlist_div.childNodes[i+2].getAttribute("id");
            call_list[i] = 'participant' + i;
        }
    };

    this.switchCaller = function(slot,state) {
        document.getElementById(getParticipant(slot)).style.visibility = state;
    };

    this.onConferenceClick = function (){
        conferenceMgr.onConferenceScene();
    };

    this.onDocumentClick = function() {
        conferenceMgr.onDocumentScene();
    };

    this.onWhiteboardClick = function() {
        conferenceMgr.onWhiteboardScene();
    };

    this.onSharemailClick = function() {
        conferenceMgr.onSharemailScene();
    };

    this.onChatClick = function() {
        conferenceMgr.onChatScene(textchat_div,textctrl_div,commondialog_div);
    };

    this.onLoginClick = function(){
        conferenceMgr.onLoginScene();
    }

    this.sendBasicConferenceCommand = function(command,remote){
        if(!basicSignalingSocket)
            return;

        switch(command){
            case 'JOIN_CONFERENCE_REQ':
                basicSignalingSocket.emit('conferenceCmd', {
                    command: command,
                    confToken: remote.confToken,
                    userName: current_user.userName,
                    userToken: current_user.userToken,
                    callerName: remote.userName,
                    callerToken: remote.userToken,
                    callerTaskid: remote.conference_task
                });
                return;
            case 'INVITE_JOIN_BROADCAST':
                basicSignalingSocket.emit("conferenceCmd", {
                    command: command,
                    confToken: current_user.confToken,
                    userName: current_user.userName,
                    userToken: current_user.userToken,
                    conference_task: 'CREATE_CONFERENCE_TASK'
                    //conference_task: task_id
                });
        }
    };

    getUUID = function(){
        return Math.round(Math.random() * 999999999) + 9995000;
    };

    parseUserinfo = function() {

        if (!login_user || login_user === undefined || login_user == '')
            return false;

        window.username = login_user;
        current_user.userName = window.username;

        current_user.userRole = login_role;
        current_user.userMail = login_mail;
        current_user.confToken = login_conftoken;

        current_user.mediaID = '';
        if ((current_user.userRole == 'anonymous') && (current_user.confToken == 'NOT_EXIST_CONFERENCE')) {
            commonconfirm_dialog("对不起，此会议室ID不存在或已过期！", "", "",
                "离开", null, anonymousLeft);

            return false;
        }

        return true;
    };

    createTextSignalingConnection = function() {
        if(!conferenceMgr)
            return false;

        basicSignalingSocket = conferenceMgr.openSignalingChannel({
            channel_type: 0,
            conference_id: '',
            onConferenceCmd: onBasicSignalingCmd,
            onMessage: onChatMessage,
            onLeft: onChatUserLeft,
            onBoard: onChatBoard
        });

        console.log('++++++++++++++++++++++++createTextSignalingConnection+++++++++++++++++');
        return true;
    }

    this.startUp = function() {

        if (parseUserinfo() == true) {
            commonconfirm_dialog("别急", "!正在努力为您连接服务器...", '','',null,null);

            loadSocketIO();
            InitUserlist();
            InitEditor();

        }
        //createConference();
    };



    loadSocketIO = function() {
        if(socketio_loaded)
            return;

        console.log("+++++++++++ socketio loading .... +++++++++++++++++");
        loadJSAsync('socketio','/socket.io/socket.io.js',SocketIO_LoadOk);
        socketio_loadtimer = setTimeout(function () {
            if(socketio_loaded)
                return;
            console.log('failed to load socket.io');
            failedtoLoadjs();

        }, 10000);
    };

    SocketIO_LoadOk = function() {
        console.log('+++++++ socket-io success loading++++++++++++++++++++++');
        socketio_loaded = true;
        loadJSAsync('ezcloud',"/ezcloud.js",null);
        loadAudio();
        loadFS();
    };

    reloadSocketIO = function() {
	if(socketio_loaded)
	    return;
        console.log('****************** reload socket io ***********************');
	window.location.reload();
    }
    loadFS = function() {
         console.log("+++++++++++ fs loading .... +++++++++++++++++");
        loadJSAsync('fileserverapi.js','/ezlibs/FileServer/client/fileserverapi.js',FS_LoadOk);
        setTimeout(function () {

            if(fsLoaded)
                return;

            console.log('failed to load fileserverapi.js');
            failedtoLoadjs();
        }, 10000);
    };

    FS_LoadOk = function(){
        fsLoaded = true;
        console.log('+++++++++++++++FS loaded success!+++++++++++++++++++++++++++++');
        conference_fs = new FileServer(fs_url);
        loadConference();
        //loadJSAsync('pdf-js',"/thirdparty/pdf.js/build/pdf.js",loadPdfOK)
    };
    loadConference = function() {
         console.log("++++++++++ conference loading ... +++++++++++++++++");
        loadJSAsync('conference.js','/conference.js',Conference_LoadOk);
       // loadJSAsync('pdf-js',"/thirdparty/pdf.js/build/pdf.js",loadPdfOK);
        setTimeout(function () {
            if(conferenceLoaded)
                return;
            console.log('failed to load conference.js');
            failedtoLoadjs();
        }, 10000);
    };

    Conference_LoadOk = function(){
        console.log('++++++++++++ conference loaded ok +++++++++++++');
        conferenceLoaded = true;
        loadFileJS();
        conferenceMgr = new conference(that);
        if (!conferenceMgr.getConferenceState())
            WhetherornotConference();
        createTextSignalingConnection();
    };

    loadFileJS = function() {
       // loadJSAsync('pdf-js',"/thirdparty/pdf.js/build/pdf.js",loadPdfOK);
        loadJSAsync('color-picker',"/thirdparty/jquery/jquery.colorpicker/jquery.colorpicker.js",null);
        loadJSAsync('ez-func',"/ezlibs/EzJsFun/EzJsFun.js",null);
        loadJSAsync('flist',"/ezlibs/FileServer/client/filelistdiv.js",null);
        loadJSAsync('widget-api',"/ezlibs/FileServer/client/widget-api.js",null);
        loadJSAsync('ez-event',"/ezlibs/ezevent/ez-event.js",null);
        loadJSAsync('drawboard',"/ezlibs/whiteboard/drawingboard.js",null);
        loadJSAsync('fs-render',"/ezlibs/whiteboard/FileRender.js",null);
        //loadJSSync('ewhiteboard',"/ezlibs/whiteboard/EWhiteboard.js");
        loadJSAsync('ewhiteboard',"/ezlibs/whiteboard/EWhiteboard.js",loadWhiteboardOK);
        loadJSAsync('whiteboard',"/ezlibs/whiteboard/whiteboard.js",null);
       // loadJSAsync('ez-whiteboard',"/whiteboard/ez-whiteboard.js",null);
    };
    loadPdfOK = function() {
       // loadJSAsync('ewhiteboard',"/ezlibs/whiteboard/EWhiteboard.js",loadWhiteboardOK);
        loadConference();
    };
    loadWhiteboardOK = function() {
        loadJSAsync('ez-whiteboard',"/whiteboard/ez-whiteboard.js",null);
    };

    loadJSAsync = function(id,url,callback) {

        var script = document.createElement("script");
        script.type = 'text/javascript';
        script.async = true;
        script.id = id;
        script.src = url;

        // alert('loadjs');
        script.onload = function() {
            callback && callback();
        };
        //document.body.appendChild(script);
        document.getElementsByTagName("head")[0].appendChild(script);
    };
    loadAudio = function() {
        var audio = document.createElement("audio");
        audio.async = true;
        audio.id = "message-sound";
        audio.src = "data:audio/mp3;base64,//OkVAARncUUyjDDlA/Y7jF0Ykw9WB2qmuqHYWA3TKNX+cPHidGYMq1lXM47s43HSc5+ksJ0YllxAEsUMJ1DZbPphaZOiB6RAvYbLaHNKlNCeJSeJTSm8po8J3hH+8NA7I8J6cJ4IDsBghAdkrx0U0//5TS9NP+aV4TxKIeJvFN0ulNNKaU0puhPC7uhPpTT/+UxZAPnCYWdWByH44kFho4vXRpNtMIAMB8BgEzImOgmWDBYMAuPAgVbhlIE21GF3sVWT2DBG0GxgWBMUMJ1Hh9yzBI8BgQw+kf//YKdmQOA/+DK05oBBCAUAAAAA44ANbdUbklkuoN3ydRXeorYcuSxDA+T7nBhC+/Z1+9/6WCQAQdFjBxFSITuT8L7OFTD//OkVDAOpOs+3yWDLAhYhnVwexAUnCrxEJiR9NzQiSucSv00SuwhAiAwNhFcwMWEAAothBBgHxAJwOBAifOHCcnKLe78V2j3mlbT5D6htOTr2hnsslGL8+qLMVbIBBAAh5y5P4GflgfzhRcQwTA+T7kgmIZ+favb3WQCi8B/mCHwoAAAAFlb6cckjkh67KpFL+0lqPDgm3i+LeuQ4D0Uj9JpuRADWIYnVAERFFy0yBjjl+xmQKg/D8uhMQJILNrmdq9vWM9cm6tvuccjkzhHJRt+MZ7KMWLf83SSzfeYb+ip8pZ9in1es9x1b5OY5V4fppDHZ+1LKtJK6/3L13tbO1SWY3bqXbtmbz1rHHVTVTwuHRoDAgYLqCIfDYWTNGls//OkVJsWURU6z6xgAAvAhoIBWMAAmrOM2mgp2NbSoibvA8nRUpKmXoNlb6aWmyJoAAACqHqtNIrfalqPDCG3i/ITDEooMHGSPZxACwjsQcXADoLLLxJcO+kmOs/AyrghAKEjJ0G00+JJAJYkS4ACU4IpJJbdrbbZDG4zElpqXxB/oMJgLxprOtUh14wQROmlXq092mWwhW4xYAc0cESAEQLZrFTBjoRDq/YSbTjGT15tYSpe/sDfbuAwCTVLeJiL8Vmf+bXbGLU7PZ5VIHic/O37MZhMUd2GoL7SWMa2FBNxiV33h1zOlx+M0Mhwwp8LGGO8ohAkGcdN5KHCXSKAoiu10H9uW7t77G+71+Of/Ty27O1tU2dfPWU7PdpalS1y//OkVLYcfZNFL81sACex4ooFmdgAvW/K7j//zev7zX75/e6z///n/z/3/567+v33n/fax1d2hVjXESKiOQ6gsLMo0OxIlsgEpwRO220AQQASGnpYgtNS+IP89RMC8afTrVIdeMKCnyunywdnTLYQuYxYAc0cESAEQLZrFTBjoRDq/YSbTiGT15tYSpfDr9bt3AYBJqlvExF+KJP/NrtjFNRSLPKpA8Tn52/ZpZDFIdjUfypKS9NVJiSxiNz7MdXqelu9dmhfLCxL6lFyzytHIEgzB03kocItPRKIrteCGblu7cDeaCEFofsJ3w1lp3yCANUAAVKmVfo1attOQxlH/le4ennIhmnvhFHoAU4iOPft8GILcnoGAZFZwBVmCfCp//OkVC8PHG9gy8xoARbw2soJmNAAEoD7OxeNKIW94KOz1PlRY4c7N38PzmcO/W8/ScMIFrM5ghlzpy+pj11g41hIPP6f9myp23SpnoR+uRU4zJViJ7FMYWZYl129aivURIJJSatrrbLsgABABEE5DGpv4b3Gpc5EM0/Q4D0AKcRHHv2+DEFuT0DA7CzYGmwQoHQ6hezrXjSiFveZoDIpXlRcw52b7h+czh36xfAiHDWKymUObSlOKbZfX8CZ6CGmqBgxhDjgQFhqSWpVaTakltyU0Ds1nXl6tCiocKIBEaSIDN3tYeZyW6SENAscHME7e4DO5mPNuY0CEjgaoARGS8TySafj5NJt6dnHLKd/u5jPuqtma1QVs967rfM/3//X//OkVFoRXKVWy83wARuBSq4FmuAA///9fhY7+v7dMyrJZZ3JX7Ki3ZCvZnHfI/vpddajVpuZ2eKqizrh5Bgo4Y0YxW80LMgkomREMuOpNKQAAJE3IZoGU1nXl6LDCRYIqRXrFzd336VKj0sIaBY4OYJ29wGdzMeZcxoEJGjSwHBJrDFlvRT5NJt6hnHLKz/d3LfcpmzNaoK2e9d1vmf75/1////X4WO/rXaEMyrJZZ3JX7MtwI5IIfqmAAmYACCgjJKrSUm05I/GKV2qJ+FfLsfQYBScMAEEOBVc6VkajmVSmAzi0MUgKEiBDpHLRGYGAQhAjw4GIoRUQuWimQUsFU0LxkiKDJ4ihQFENy7WedFTLZFpMGiZMoHXLqDVty6X//OkVGIVJTdOe8zQAR+h0qF1msAB1IIumYniHF1kknVbVWZm9BAwZ03UiiqtkaDf9JMzRQL5ugbpushxEkjI+ZF4mpNG2LfNpc/VLFn/XR9diaXTq90Uax1r1vWkrSgi402k3IwAAEBJYcpXavRRPpUjsDAUkAGECmEbGBXnJXCRYyAVoxqqzlCqNQC++L+PZJo1einIxGV3yaVvzQWbFatxd8ahyOL8n4zy7e7jrn/jz5RLK9Ly72lw5+///pae5hj3OZvRKlvZZd1////vO3+GFTnc+6q2cOgowI3YIfqhmAYgALRFAU4ZD799G5JJLMNPbexgqneZ0YxMtZrxGL205jYKgeOHnnkM2bpuHuQQskM6+9W5DNP4CG6Prr3L//OkVDsPFGlS28xsARYAzqV1mNACmtzFvCcp77+SvEE1C6yaDpo/Sw8h74OPv26XtIN7Cay9tSqaikX/eN623ytL0HVIfW1iy2mKXZiO3t5WnMWiKCqGQxltNpBGaQYxKBbO4Kp3mdGMTLWa8Ri9tOY2CoHjh555DKzcdj7QB4hD381YhmX8ARuj66+VbLdBnhOW77+SsFA+FRY+8akE9SZlMuCdOuWjyqhswI1YIfqklAWAAEGFalVqqq2lJLbVZXXjnIJLjr5IjTvSoH9JiTTdDbwoCZ0ZtEAnkNPkR7eMNbjb3Iqj5mooZcXoMmEgMhRpYKhknTYcZh7q0NLSUk/b1LrOMzFLNy7N3aCi1P1MKXmqtmtYw/9aqWbcU3nn//OkVGsW8RlWy8zsASGpqq4FmeAAWtdx/uOP6/v/nnz8+bz/HmrW8u633v///+cQimqTkOWPlmNvmu77lrtYseJv8Th/q1iVOj3qfXLmXtnn5ggFEiJYoptNZRiSSUrF1pJCZSVjaSUjdcsoAACR24deAOQSXHXyiybpEkckmJNN0NvCgJnRm0QYDmBm1hi0uMGm4zeciqHzEIIMOB1Q0wmCYSLAlS1DJOnBxmHurQ0tJST9vUus4zMUs3Ls3doKL5+phjzVWzWw///6lmXvxdt09bLvP/8f1/f/PPn583b/HmVoTDib9e7A//lK6lf/WmAFDaQAVVsAADcmgCSE1G3YyZHdNp98Q0wvH/GSMypOaCn3oPuotr0/jZJULrJ0//OkVCYOlIFnL8fkARWI/s4FmMgCnqal3QoJQyuLy7L3Yi9jTkTH41eVbde1nqteqc387lv///wudPNfKP1dfbOL6i9l7V2UVmaNYflBn+nrqg+/4uRH1MK2Kub7PAjpFmdpSlk0QSSmo27GjIwAIElcj+tQXagm9FXaqw3GQu3FdtK+xskqF3FLT1NS7gkwCz3Bi8uy92IvY05Ex+NXlW3XtZ6rXqnN/O5b///8LgNLTgn2dvlLew+qwUnLTJM6ZJn/1pomWxqv1mUU2GegAP5HrUlLafHHKbpKedntdmaWxhqO2Idl0DNef5QtK0EjRrdgC0UAySQgaJcAJzgYwGwDA0DV44hzCuYFwwQNzNCmpBN0FJJF1A2OpIX/Wv9e//OkVFAQjM1Su+3UAQwIhqp52tgAvrNThFk0Sus+yKK22UbA0bIZ7BU7/nud1gqGsSw6KPAT1PX/mv3P2el5s2fdUXLipBaxUjDspgBAAABSulpbU3SV521rszKbGGpmxGbUWd6mfNxUERoZxN0GemxJAgrDMmKg4oDAqkpjMfqsa2TLAACu5Aq/bkhucR0SY4vkVq7NpFz1z1hx0kxqMyVsSnIXMNEgdFABIcYbcwGWaHFY1PY2WApIrSafLn2ac3FkrAn7gXd6IyyXHQycLsomdSaimwaecRrIZeotNWtFGIB6ldKsu1EStJthkNIiFmQaCuM+0X9ygux6jp6cUong4MLg2IHtFBK5ALmdj0krAcCRI482xZmA0cY+mxxl//OkVJoU+QNIyzMpaQpIhp50SzI09dYxTJNlKBZqkMVpeIVeGdkIAAB9RrBhJw7qKy3ZtCXLrnrDjogrlqGQg3CqRrmDpZhGhihx4cCN2CH6qkAZIACpKU5JbbJIEnsqOtQWKBhouBVacoKkpm3+nqSml7O2syaGWcvkvhLKKshbuCKq0QucszeslhOsBU0LJkaISUEkBO2sAVw0wQoQWBFE+YyOBZsrJCCKhqLEnjqInColEqw+IQvzKj3puEwGo+E47KpgDZUQTMQUMuHR+SYi8ywVmwlMWGi6uSmNoLXOWDk6Y2v85fObccYm8669hK4U+ek+hJI6DONc7/bql3Xn9f/1pz+aXOr1S5v/fb/d8s+t3voSNxtAAABe79HW//OkVMoYATs2z2EsmIqIhn38wzBsoLNRhouBVaiKTBJTiUqcaPxDAiakkDo2bouKWui9YWS6wIs4IfqoQIZgAO6XbkkjSb+LxmXw3DkJVWHRFRAc+A4acZ/rNLAtSXWr8XgLO9LnSgbKXyVpLWXVluf4XH1rK4jHRKSE2jxyeiS63EP50wzAdwN0E9EuYQdPoCaTB4LTqJsknvGSpkmLlz45KhJLPLoy+MhPHmQbRgiEI8CS0Bof0MmrAZFcOhcDUml4GxbEVSTR1YEpeelAuHzxaoc1TI4Vt3WTnI6++s//cqxRvo+uKDsyAmTCvomVUEU+PJDotZ92yy4f32vKKnfLt+IZUpn+DP+MFrraQTvwVkBjtgBAAEYrOowuFJlM//OkVOAaOe8sf2GDnomwhnH8S9gkewdwpbeSgqErmTU4QVVkEhX6whG3mZfBWsCbyCH6rFkNJAAAcAckkt122trvJlonIDTGEYU3rO3ekjKnzWgyR54FdOBkw2eNMij0tydyMRSgbSvSUV+w1izD9LF6+EXlDiSuvhSSGlycNzbXLHh9bkQkIGWkBgRi66rjkMXqFCg1C1goIM2pLiBSKzeFmjBWTJspa9MQiyWLvMMMXJPv2QXOJnGFzlE8ziAxCaTDKO6SaiayF7ltqvR3HxkTMYIFJPIhPckREucWahzzMs90fUisXhXUdFOkUAM5nWIyoGj6gcyI0cjPU5fgiUERZCVjAAAAFHmZsOStAQxwyzVTsGXhvG2SAvCPcVO5//OkVOcZFh05L2EjrAvwhm4Aw9ZQAD8MQNA4D5HMXBWLBgGiaC1hwJ5YIfqrMtgIAABdpuS27aynOXQ2whpYB/IlWKI0kc2lK3I3SJyhrDutlMhsCHGKrEJreORd2xT2UjoeDfEF45LLRwekxVCqfRSZZrFnVsnjC0Ta9rq+tr7S5fYjxtE3M8sqRaHx2qsPy7YYhMUwmVeWkvyWdryZV3kaxa1U2UnK9yNRB0nK04Pk9ol56mWLnSO28vQvO4qqMYtKjjs9mJIHlwRZI+GztwJw6RycScuM5bU+vjbFazonFy/+48vvt97XiVzpZi+xK5JUyF27pLuCKPbPtPNQbJcm0AAABvj4K8SU4B/ExOguR1I6UGKYsyKdMVv60PQE//OkVO4Z4hk2jz2GnomIhmjce9gIThP4PWaTnhjArGgh+qwYyzbNgABBb/5JLbJC4I0p7JhjbTwmLixrB0XMdxhrTCl2k3Sfl0HS4i5QSfKh+lqwTkTyvkO9zbFZBV2Go8G6IzK/JdGbTntc5Vqzl9TblZbhk0cW/Wb1bMwZ2FqFopKPpKlk9tf2DGFJrozuNzmqpqsihK2HzRMRpA9gOuDwog5XmqZLa8eM0spTKJIcD5EkNwQyyLCk9Fc0B1dVX1ATh8lfVkzsslScvdSxzD/2e8xeGSVrmd4dXPNndnd1BUmT6/773ePi4M5uv06aqlB4e2VRjASaksACAAkMhOc+E+uSkdEJRaoOi5jsrOjm1naU6O8mAZKVDChAsWF5//OkVPcZxhkyzz0m1oqYhm38S9gYGdnA62ghWqH003f6Pppu/0AAQCauSSSRsICJsVqtF2HydkFvURkFiNqVECShQjrhMakQo0R8HssvGIw0bAVMDqtgl2ior501vX7UjMGQV6saDOZHFzbnBshTObKq5ZoOWQrI1Zl1JDhQNwD4XT5+EF5MKhofhIrJtC2t4F5LYm9dScMKPKamLrLJIXXNfD+dLSJZVFtGryyagcmRJI0UteB4W9lNOiYuNYCMW6Z8oMemXQTkku/1vrF5tzM23hK9nc1C/BrWM7Y9+quJfvrvVTrW/aNf/O+bjRtMvNsf//7kg81AAAAfJbkAoQ2humAwr6iLASZ1mAX9GULrhMakURORXDCWWBuO9U4Z//OkVPkZvhUyzz0m1IsAhoH8fh6AaMCeuCHwpEhU5AAAH3ajt22+1rwpXyx9mKIPiAjVV/srl8siu78ZcaTQl134lclgylxhb1xGhqQbikLJ1jxjrw+FeJ/1EBZLRfgjMD6uG8Ti2hvFswrCkesNpGEqxlZDxLY2zrxvinHKoN4V7UauiX+Lmpa89Rm2zCqXqDPzwltlhMhB2SFpdicdWGL3QFAdBfAdQIrSGZNtIcwTCWINNOMHHmiLBYsMb6ioZCR4RmZL9wSUSITUMW5Sap5Gp9xa1IqI6GosRMDnRpeUXhLuWobQ4bo62N5Sbho4HBlf////g17gAAAF1BjqxPFYGrCSJk8wnVnge3QSSA41GIMwoOxNDQxqEY1jylgG//OkVP8aphU632GIjoq4hnFse9gEnsCdKCH1oJhtmYJAGbjlt2231p1OJ6TlGp1sUo5XNFqxIH8ZB4ohaJQdTkQVTm4xbPBXwYzct6L+aUVATFyjJNUyUfnsh6kULcwQZVRrGS+tquZro1/eK9cQUA2gTEpCiw62xqEQyhJow9hmQpUZTVTXQYXtR8KZZZiQRV52nwtOcCFhEOMk9S8Gno2GcXIrIzEWThRCcJD5kpWpL4rM4RkIqSR5FvU0lnXU4IJ6+pqXMzGM6a8WiprNwWzAxcT6yU9TI/8s9yK0RxeEdPciTkCeo3TwwBE///////////////4s02AAABLlkoHIXc/1sWo5TzJGaCYP46BaPBaHAnPAif5IGJ+aifls//OkVP8a3f863z0jyoqAgm1sex6E3LbAipgh9aDAQAAAQWTlltu2shlj1HCQZEnMXVWG6g1YYZqMaNPJxRzvSHJhXEsMuC07Vs6JXEAnAd7UXhRk1etbTSLo3mlJMiwdtqq6VibCZTxn75PsbXaTL4p5cuoce9fFjHsWKZ4o7qE1R2oPNKM6KWzIKsl5JL0XVInMDopg9xpsVpimAKMGi5EvJMYYmeH7SQrWbTSIJlEi8rPufgxVCbFbIfPIIYNw1eKD/aU7L+laevJPjY4Z14S/Wx9tIZHRSilCPNj+K7RtuniipyOeZsFkHW1///////////////+TTAAAARwDB6IUbpbgzTQE9QacMM1GNGi5SPyqhKIRkAwR1ps0tsPJ//OkVP8a5cE23z0j2IpghmTcY9gMUWXAiLgh9awGMBgABLacctttsgtguCqJ+OMTOASwhBv7PiK3nUcipZTEEwPw35i+WuyKh8r1enbCfCcToFyPJyX4Gjju1pdJQjUgVpivlN+K23cory8NuV8Mh06+wn9SjVi0iLaUnVw0anNEe6orUgDbz6IoQLMq7JGjR5Ba3kjju2VNsyqbRI3KCkm5OQUmTo/IZIzqiJQxpJPnom6VqUJEbNRhEAPE6ozCqiaXFw2PsG6zaUMgXZUvzL1Gmly00lkxQgHPuMi10gmY5LkhOCz4guXM0VE73lnpnGKtFv//////4I2WAAAA9AuDMT8XMWNIAwCAdyJjqcnEg9UmQJgch31Pzd3BkfM6//OkVP8bDh003z0o1goggmlsex4svTvAiOgh9asCAEAAQWq+Wy2yQcQC0SAOZcQyEwUaSxbQl8pVgtquV6GoxtORyG4VzUc92JniLtwGyiV3Ed3YTu74fSei5MXi73ZyNDugNr20UR+tbOQ+B8PmUOC8USRZMA+nPt0xR2fJkqmzMtpqrETpC06Q3TqUW5QpK71mLYWEGbl2C7V3WW29xYpZeZR3it1mzPW+p+fUutzVat6HZx6sWnPiI6VLRzJKl9oqDrLdmzPPlyWoltu0fDgVbMzuZ59t/uv7pouqP0j83C9mvVIZVWyo+pf///////////////oNx24AAADqE2OwRoNABZFoUKx1fKVGJ18/NFGLk5FoSQj14t908q3h//OkVP8a1h00zz2GngqYhn38ex4IhqjAiggh9aBBBAAAZKcktuu21pbUwOdGCxEJSRe0Ck2pTFKfx/qxDoballUnjMVB1qM6H7xnYS9MJKDkRNWydxhTqFCGFrVzc7d403uHj99BrE8aJieioMFG9hgw6ab/8nd54V0/OTrMD7Mr5Yxh1hsD4rqImLl/GJqTcovTETS0mEU/FkngSKyaRk0IvVR0o7CPVEvNXIOmjRagtDOQlcj7SyFmqgkgIVdVM27bj8mkqRXFokpldmKh5p+k98VHrxTZB3Vbx9aVvGW+Zc8/4xU38bxD6yM6H//////////wFZsAAADlPYYawWI0UkXs+ijPQvxSn8f4oCx06EhMIg0Jg7k8SF54fnIM//OkVP8akhU43z0m2IrghmlseliEncCZaCH1rSYMGAAABqv+S222QWhfLmN9VJxNKRvPFnfLTAWDRfFeuSbLKJNxkH85HPculKn0r0KT71VLhXy0W9ITImZbNp9u3h/zQWXCG/uNVjVYNnp+lJK2pVkZoq5Yts6fq0bJscyR5pCWVRRBtgLuXd4o0nTj0DTq0OMkaBSTJZCqfD3XGF2Mi0xRsmI1ZEz2l5NQQImtLNSTVLVEktGoVbm2udsNEkEEqY6+ds/ORC2vGG4jICf/Uq7Bm5dr0v3T1zDL/Mua55aVZONmM9Rd0QKhv3w01Lb/fVAzKn//////hW224AAIABYZPwTVGAbBwnGJ+uH44CjgkXr5NlhIoh4ZTMn4QUpx//OkVP8bBg80zz0m2IowhoIAY9ikQJ/Aivgh9aZgLAAAALicktu22tp3g5StJ8S5CoJeZSVMQQghRezvWZIKnPxiRkRCTncjOunjxel5iLlWQUFiBVgYkONepfUa8ZGKZzUrSxSZLt4UHbHvUfW1IUsRvaHzbCet8N0nCSKjckaUKWLkhLNG1NpFG0Ag6OKN+QilJEhj0T8ZSQxWLiGKJToWFV3EZjuWaqNoWZB6zMfPG9RLIh2UZKrhCJlJWP5iPhzlJxTRjY5+TmaopLbnlSrzSflPWDOv25rvEFVb6fzZd296v/Nmpid7PO953NyUf///////////w3LbsAAABcAI6SIcRxQThyf0IgAjFsrkVpk7JCY0cLJLeFXlYfTk//OkVP8bCh05Lz0m1goYhoX8S9gIWuTAjlgh9aoQIQAABaTbklttsiJVAojDOAmWBPTyDNQRFqEhBhHeu1IsF4rkzDiXBiN6sRadsm7qQvUc829WrxeJJmB5amCAsLaoJosI9PWh3a92DhdzfxjQeJk57dybNzo5HRhA3kCndYOx8Sdd7j3mHHyfEewwXgjgpp0/ze07nokP07F17tI7rWW3lEUZ91Vf2R64w+8xNWth2A2uuc091cwSL0KQtPDBsxzUlheeMOaDga4L9b4lOcmTqmfO3OVvx9Of0xvdtiL1v6PVdTt8vAynYPwG+f////////////wChsAAADlWBRGGcBRckKOJCljXVJODCO9FohwsboaBUVBYnJA9kmxH//OkVP8a2f803z2GnopAhmlse9gQhsCfqCH1pmYBmAAAEqTbktu1skZEmQTJ+tmKf64RpcDQFqPw/0SYp3nSn25tOJ4QdiRUQ7ULZGlQKo6Vs+5UKVr9tZ6Zbi8JAiD4WUhS/quKXisvk9XOzVGPQ7naXUND1M6/cuqfp7t8eqfnxNXuXbLxTdaiYE+jyMvLkiVIsYOEF5qPDOwd0KaQkPtGaRPLyFt3FdoqWpHKEeUTxLfecbceiut+VNOtj23fQzeK89S93G6q3NYtC73WYXMmzcYzNWnvmHLIHaRNqbmrYJaxmRWEkEQ8tlT8zsXhiMav////////8qrZAAADjIEDufpcZo+z0VJ0HQOpEKdSoFJlxT7UuTieFvhIriR6//OkVP8bEhU23z2DrooYhnFsS8wYCyHAi9gh9aSAEAAAS7+SSWyRaKgRCMqAdtCxob4ypz8Zq3arzV32WJJSFuj/SOBL7uw7IIHg6MZ1G6TEjsQiQTatHeNCoOA6k47aXmyVYEyh0+xccPJXYkgf6d1TY8n69liyk9HfZZlYrhi+9S1PO3gs/V5ZVdDV7zqN7k9oC7e8RSK2rX7dC4sW0Pyq8oXQ9Ve+wl6D7KcVymrZTLFbpyqXT5K514LnD3QIygj1aBnP0HRKn3kKWZKVtXx2LLTtGWmzEsJGsXTJkYInWQt5OlvDTSj9kglqcK5f//////////8EqSSAAAAbpbhOxoJ8hhrpliK+q6fvm87muxkCBMp6CURxYqCksqCO//OkVP8a4gE0f2GGnoqohnX8e9gUaHjfwHqYIfWgAAAAQJL+OSSxsV8DKSIWJiHiZKSOIuKqVxOzoLofzw34L9huVRlKLKmeHIj3BDXNCidpBRIceT0eY0zR+VMGgsL0C5uWqv3Hliz6dRMDjVCuHyzasKEpfes8qbdapY8ctRjhu+hMtumLiWFe8wxKG63DdW9DE/RZebw2aq/ea31G+ni1QoXP0RSZqDk8XKolNGEX0ouRQNpDujcxLGyE1MloOl0EMe7jQ9aEHpZSnpC0ty9GjZqmtzFZBXi4lLrZmNqlXu6NkQnJGLvzf1bhLiv///////6EdtuAAABPj0opRWWSEncTYuKqVxOzoNwvysH2MgmAqMNAqLXkpYOA/qBF//OkVP8afeMyzz2InosIhn38eligQcCdmCH1rYQbAAwAUubUtu221p+rBARalUd6AemQRCKUBlH8nz6W2ETE4UXBPR2cLOnYqYd1jRXyKUzM5qBxVrKooKbJ8rzeSH7RQRcUKgIa9Gjq5ZUSE4yKmTpVNxIq22oIWFLQmRnXoEVtMym2oQtmNUBWR1yaOdepeFJMMM0VabMEaL1qNFNA5jFaihnA3Oj/LMpahZnHJwhUlHqt4ilKY05VHFJa5RUtqTDCeGZp3NVlcJ03M0FGUyinM3cdAUgiPaTIRFsxcVw52EhIWdbRWIiEkcf////////////////AZqAAAcqwQEWpVHuYjKSwiEUoAqHIljY3SiYnCf8E9Gk4WdOvTYd1//OkVP8a6hU63z0jropohm1Uex5kZorAjOgh9aoYKAAABuLktu221tOzNMJH5fcBiRLkaZzRvyvaWOsrjkFwJRTsvru86FynsPnAkZi27Tl01PnfnY1G5E/buSJ+2fHJspOPXQQsmbiNL9o3rHKsvixWllQrkwijuUn4YH3GHYO1ksI2lsqldHjtouQJapolz3qFyp+H9s/lfb1QZnRwhuLG4FkBwbrvOav16NEhxyvihpW+rVzBpBfqU/MFTYIprQxnopYQMMJ8NdJOpRK92bow2yowL1TVlbtNNIJUch0jD39yT9U0o7NbemYXruvag930u6iHlf//o/8oAAAPyZhzDiMNdhcSPicu0JL0yKoWCqTCBEWS22Iwoxe4Nx0M//OkVP8bBh0632GGrAowhn1se9gQTarAizgh9awABgEAFWqv/6BcAZAIhbnJD0CeS8LQjiFGtZ+OE/06cCnRt4CFN+5sl11AhvV2WCkVSn46tl+2MT9aP58qm2DRx7O1prvI0WG/gwJ5GEbULUNMKZUKSDPDVwfLko8oRi0WCpMJURKUMLHh0iBE0ULOWEY7JdK1CMJyhMU/AeDcicVCgoRFrQMoVFLeqYinUU1RO5OGlz0IeCJe0c6ZwpY9JNxNtqlFKP06JQmGNQLNqSQD9mvi+t9T9Tp6VUlpXxF6vpQ+NnDyY6Qa3ah2oRJtcnX/////////hll2UAAABdBcBaBNkcTtKoJuHQqi5DL2wCksxH4b5YWOAdTXabnbNiV7//OkVP8apb84ej0m14pwhn38ex5kwKw4IfWrMYbMsGYAFlbdtl221osw9BeBjPT3DBsehYTpNQroioQMduIKj3qTO0kx4RmPZ/+J44kxYly7bH5wKRTO1A1MSCXkRYikDZlD7JruSETa6JiIoDjIppr4dZJ4DIgWmikPqo4CokZXTS1IlIUZDMF2kSIhXyjMZzYHIrcl93WkcF6JVVLIDEOwNJQ6VTVfzFLr88yhVRbo9K3GVbPSLtlR9GhiwxNiT6LJITlnmE2rQIsgDY/Jh+x2wjMUzTZjPPdmtXa6ednOqD2Rzf2w28taYKGs4iLkf////////////Ea4gAAAHcTg4CTRUmQmy8hKtShDnBUGrHUxbVuKvnaOo1Iyv0H6//OkVP8bGf063z0mronQfnlse8wYCcCcGCH5rSaCTQAAGvtt23//+xdE8NwQ4VxPnAkyDDNQKqLWGrCSKjJ0mcrTzUrEz3Q1PXT70sZ/HGrcwHy4iwDnVReVwStaWiABRshAVhhAIAPeKIlGW5o9YGhW468bbFI2ZkmDkksWMFOpWoXnxLMxorimXXJHIlLRJUKCMpRlEs3SPEZ19sKIkpEck6ZxAuTwK7K+zKgOwUIV0c1qNIIkmui5NVlOWZGpXhDPxNhBUPCCMIFck81jRyHkt4nJLn15KplStcp67mS75gujWPPmbPd2Pcsue33csuOdgxb/JCTloAAAEgOYIYIcMBPqhrQ4ZqBVSDy8JIqKqE+HqPIJ0f3MSVcrrQZj//OkVP8aohU+3z0mrIqohn38e9gUl8CZ2CH6o2SDZAAADvltu22/1pooaPUW5lTrgfaPQaeOYW5mQ2KS4tx2nKXFZUj9Ip16okspTJOlxgs752qlc3DRMbJoT0mGoA6MslBCKShbZN2zrcjhx4nacTNsoFjbeQ6JFCDSMhcusRSQoZPVbKssU+fzZsuySJqz7REW23vQiaRmkSapEiRE1KERM3IBgrCCFlZsq5HsGUMWtJSZF+WOwahgJQFR6AjkgZgeBMKZSDDhyl1EiaepyMbxmuU7SzaQzyi7SG6iWYveiXQnrjFnRf////////////////8BtmWgAAADUkgBAkeklQNx/KctxbhblchtScShSIIHSKcLyCSVpaIxyFIi//OkVP8agfc63z0jnosAhnX8Y9hEqcCZiCH6qJgi2QAADXcttu3/2xzjpFGYyEpoJWLwH0llEoS9weaJzH2fclGw7zkLhlJKtnTavgrg2SUHneOd0VgKhQYePiQRNnR1QDaJpQiK+NjQYxRY3EUigEiNKZFoCtCA45HT3oTGvbJC1MOQXoTFaMLIQfnRYVoSJZVlikAgWSEx5WLhE0HllUJsqoUFZJKRcgVppZJZs9Gotojwme6BK8RHYGlZWiCLKHGaYBzqSYjYyyKQhkln6os6rSIXcqpKDB7pkSk/RVHrNatZZTy7FPDO2mq1aaS3xUFHSvczyyXOo3I7HOGlRgAAA0tPlEtizqtmDjpACphcIw6gzZ4GohDMbsMJCjJQ//OkVP8bjh083z0mngrohnFswx5MOC57Ob9pY4K4wJ44IfqtphbaAAAJJSOzbf//7QRcRhg5iMFgLekyXFyMtRIJ0dySL4qGMkjydfSrXedPH8po8Y0nTusj2rS8OA5FexohQs1D8XT1xc9uC8fjxwgw4W/EbrwS0nf7YGBtyzMvP4r5t4q2iXSPrYbcgZJEfWgOlE+20mcUTJ/NGjqU9YOtyLsGl1UfQN919vBRGV6u6wPWJGDCWvskho9C0PgzFNVMPImVL7Xjb8MEM5nLNYgua0YJVnBBNgz5G6tW2xVZ48JqlvGKixUNnc0O4TsArMsqAObjDLAKUjgAAANohoj49R8IQaa7LkQosZ1Gs6MJFEoVCvFoeRxPGyH8ZWJR//OkVPcZ0hc/Lz0j2AsAhnn8e9gU0+8DaMCYSCFQoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
        document.body.appendChild(audio);
    };

    loadJSSync = function(id,url) {
        var script = document.createElement("script");
        script.type = 'text/javascript';
        script.async = false;
        script.id = id;
        script.src = url;

        //document.body.appendChild(script);
        //alert("loadjs() exit...");
        document.getElementsByTagName("head")[0].appendChild(script);
    };

    failedtoLoadjs = function() {
        commonconfirm_dialog("杯具了，找不到服务器", "！重试一遍？", "重试","不玩了",
            function () {
                //onConfirmConferenceTask(response);
                window.location.reload();

            },
            function(){

            });
    };

    onBasicSignalingCmd = function(response) {
        if (response.command === undefined)
            return;

        switch (response.command) {
            case 'NOT_VALID_COMMAND':
                alert("Invalid conference command!");
                return;
            case 'NOT_EXIST_COMMAND':
                alert("The command is not exist!");
                return;
            case 'FULL_CONFERENCE_LIMIT':
                alert("Conference room is not enough!");
                return;
            case 'CONFERENCE_ALEADY_CREATED':
                alert("The user has aleady create conference!");
                return;
            case 'CREATE_CONFERENCE_ACK':
                if ((response.confToken === undefined) || (response.userToken != current_user.userToken))
                    return;

                current_user.confToken = response.confToken;
                conferenceMgr.createConference('CREATE_CONFERENCE');
                return;
            case 'INVITE_JOIN_BROADCAST':
                if ((response.confToken === undefined) || (response.userToken == current_user.userToken))
                    return;
                conferenceMgr.onConferenceTaskEx(response);
                return;
            case 'JOIN_CONFERENCE_ACK':
                conferenceMgr.joinConference(response);
                return;
            case 'CONFERENCE_ALEADY_JOINED':
                return;
            case 'CONFERENCE_NOT_EXIST':
                return;
            case 'CONFERENCE_MEMBER_FULL':
                return;
            default :
                console.log("basic command :" + response.command);
                alert('not exist basic signaling command!');
                return;
        }
    }

    onChatMessage = function(message) {
        if (message.message.typing) {
            updateMessage({
                sender: message.sender,
                //message: message.sender + ' is typing...',
                message: message.sender + ' 正在输入...',
                lastMessageUUID: message.message.lastMessageUUID
            });
            return;
        }

        if (typeof message.message == 'string' && message.message.indexOf('上线了') != -1) {
            onUserLogin(message);
        }
        else if (typeof message.message == 'string' && message.message.indexOf('在线上') != -1) {
            onUserOnlineNotify(message);
        }

        if (message.message.lastMessageUUID) {
            updateMessage({
                sender: message.sender,
                message: message.message.message,
                lastMessageUUID: message.message.lastMessageUUID
            });
        }
        else
            updateMessage(message);

        document.getElementById('message-sound').play();
    }

    onChatBoard = function(socket) {

        if (isBasicSignalOnboard)
            return;
        isBasicSignalOnboard = true;
        basicSignalingSocket = socket;

        basicSignalingSocket.send(' 上线了');
        input.disabled = false;
         console.log("onChatBoard");
    }

    WhetherornotConference = function() {
        if (current_user.userRole == 'originator') {
            if (current_user.confToken == '') {
                commonconfirm_dialog("亲,有场会议等候你", ",进去吗?", "进入会议",
                    "我不", permitConference, rejectConference);
            }
            else {
                commonconfirm_dialog("亲,有场您曾召集的会议", ",再次进入?", "进入会议",
                    "我不", reentryConference, recreateConference);
            }
        }
        else{
            commonconfirm_dialog("亲,您来早了","!会议主持人还没到哦，等候下吧！","","",null,null);
        }
    }

    permitConference = function() {
        startConference();
    }

    startConference = function() {
        console.log('++++++++++++++ startconf = '+ current_user.userName);
        basicSignalingSocket && basicSignalingSocket.emit("conferenceCmd", {

            command: "CREATE_CONFERENCE_REQ",
            userName: current_user.userName,
            userToken: current_user.userToken
        });
    }

    reentryConference = function() {
        conferenceMgr.createConference('REENTRY_CONFERENCE');
    }

    recreateConference = function() {
        //basicSignalingSocket && basicSignalingSocket.emit("conferenceCmd", {
        //
        //    command: "RECREATE_CONFERENCE_REQ",
        //    userName: current_user.userName,
        //    userToken:   current_user.userToken
        //});
    }

    rejectConference = function() {
        var originator = document.getElementById('conference_originator');
        originator.disabled = false;
        originator.style.backgroundColor = "orange";
    }

    anonymousLeft = function() {
        window.close();
    }

    onChatUserLeft = function(username) {
        var str = username;
        var str1 = str.split('#');
        if ((str1[0] == '') || (str1[0] == username))
            return;


        var sender_user = str1[0];
        var sender_id = str1[1];
        updateMessage({
            sender: username,
            message: sender_user + ' 已离线.'
        });
        document.getElementById('message-sound').play();
        ParticipantLeave(sender_id);
    };

    onUserLogin = function(message) {
         UserNotify(message);
    };

    onUserOnlineNotify = function(message) {
        //alert(message.message);
        UserNotify(message);
    };

    AddNewParticipant = function(user_info) {
         if (isUserItemExist(sender_id)) {
            // alert('b failed');
            return false;
        }
        return true;
    }

    AddGroupParticipant = function() {

       if ((haveGroupChat == true) || (userlist_div.childNodes.length < 2))
            return;

        AddParticipantButton({
            NormalParticipant: false,
            sender_user: '群发',
            sender_id: '-1',
            sender_level: '0'
        });

        haveGroupChat = true;
    };

     AddParticipantButton = function(attr) {
        var button = document.createElement('button');
        var sender_id = attr.sender_id;

        if (isUserItemExist(sender_id)) {
            // alert('b failed');
            return false;
        }


        if ((userlist_div.childNodes.length == 0) || (attr.NormalParticipant == false)) {
            button.style.cssText = "width:90%;height:45px";
            button.setAttribute('Checked', '1');

            button.style.background = 'orange';

        }
        else {
            //button.style.css = 'width:90%;height:45px;margin-left:0.5em;';
            button.style.cssText = "width:90%;height:45px";
            button.style.backgroundColor = 'red';
            //button.style.backgroundColor = "lightgreen";
            button.setAttribute('Checked', '0');
        }


        button.style.backgroundColor = "orange";//"#cccccc";
        button.disabled = false;

        button.onclick = function () {
            if (button.getAttribute('SenderID') == -1)  // Don't process invalid sendid or groupchat button
                return;
            if (button.getAttribute('Checked') == '1') {
                //alert('1');
                button.style.background = '';
                button.style.backgroundColor = 'orange';
                button.setAttribute('Checked', '0');
                Setgroupchat();
                //CheckUserlistSel(0);
            }
            else {
                //alert('2');
                button.style.background = 'orange';
                button.setAttribute('Checked', '1');
                Cleargroupchat();
                // CheckUserlistSel(1);

            }

        };

        button.setAttribute('SenderID', sender_id);
        button.setAttribute('SenderUser', attr.sender_user);
        button.setAttribute('SenderLevel', attr.sender_level);
        var label = document.createTextNode(attr.sender_user);
        button.appendChild(label);

        if (attr.NormalParticipant == true)
            userlist_div.appendChild(button);
        else {
            userlist_div.childNodes[0].style.background = '';
            userlist_div.childNodes[0].style.backgroundColor = 'orange';
            userlist_div.childNodes[0].setAttribute('Checked', '0');
            userlist_div.insertBefore(button, userlist_div.firstChild);
        }
        return true;
    }

    DeleteGroupParticipant = function() {

    };

    AddUserOutputDiv = function() {

    };

    DeleteUserOutputDiv = function() {

    };

    UserNotify = function(message) {
        var str = message.sender;
        var str1 = str.split('#');
        if ((str1[0] == '') || (str1[0] == message.sender) || ((str1[2] != 'originator') &&
            (current_user.userRole == 'join' || current_user.userRole == 'anonymous'))) {
            message.message = '';
            return false;
        }
        var SenderUser = str1[0]
        var SenderID = str1[1];
        var SenderLevel = str1[2];

        if (!NewParticipantJoinin({
            sender_user: SenderUser,
            sender_id: SenderID,
            sender_level: SenderLevel
        })) {
            return false;
        }

        //alert('response1');
        if (message.message == ' 上线了') {
            basicSignalingSocket.send(' 在线上');
        }
        message.message = SenderUser + message.message;


        return true;
    }

    InitEditor = function() {

        var numberOfKeys = 0;
        var lastMessageUUID;

        input.onkeyup = function (e) {
            numberOfKeys++;
            if (numberOfKeys > 3) numberOfKeys = 0;

            //if (!numberOfKeys) {
            if (!numberOfKeys || (( e.keyCode == 13 ) && (this.value.length > 0))) {
                if (!lastMessageUUID) lastMessageUUID = getUUID();
                basicSignalingSocket.send({
                    typing: true,
                    lastMessageUUID: lastMessageUUID
                });
            }

            if (e.keyCode != 13) return;

             if (!this.value.length) return;

             basicSignalingSocket.send({
                lastMessageUUID: lastMessageUUID,
                message: this.value
            });

            lastMessageUUID = null;

            // self preview!
            updateMessage({
                sender: window.username + '#' + current_user.userToken + '#' + current_user.userRole,
                message: this.value
            });

            this.value = '';
        };
    };

    updateMessage = function(data) {
        if (data.message == '')
            return;

        var str = data.sender;
        //alert(data.sender);
        var str1 = str.split('#');
        if ((str1[0] == '') || (str1[0] == data.sender))
            return;
        var sender_user = str1[0];
        var existing = false;
        if (document.getElementById(data.lastMessageUUID)) {
            updateDiv = document.getElementById(data.lastMessageUUID);
            existing = true;
        }
        else {
            updateDiv = document.createElement('div');
            if (data.lastMessageUUID) {
                updateDiv.id = data.lastMessageUUID;
            }
        }

        if (!checkUrl(updateDiv, sender_user, data.message)) {
            updateDiv.innerHTML = '<section class="name">' + sender_user + ' 说道:' + '</section><section class="message">' + data.message + '</section>';
        }

        if (!existing) {
            //output.insertBefore(div, output.firstChild);
            output.appendChild(updateDiv);
            //userlist_div.appendChild(button);
        }

        updateDiv.tabIndex = 0;
        updateDiv.focus();

        input.focus();
    }

    checkUrl = function(messageDiv, sender_user, message) {
        if (message.indexOf('http://') != -1) {
            if (message.indexOf('id?') == -1) {
                messageDiv.innerHTML = '<section class="name">' + sender_user + ' 说道:' + '</section><section class="message">'
                    + "<a href='" + message + "' target='_blank'>" + message + "</a>"
                    + '</section>';
                return true;
            }
            else {
                var url = message;
                var str = url.split('#');
                if (current_user.userName != str[1]) {
                    str = str[0] + '#' + current_user.userName + '#' + current_user.userRole + '#' + str[3];
                    url = str;
                }
                messageDiv.innerHTML = '<section class="name">' + sender_user + ' 说道:' + '</section><section class="message">'
                    + "<a href='" + url + "' target='_self'>" + url + "</a>"
                    + '</section>';
                //location.reload(true);
                return true;
            }
        }
        return false;
    };

    disable = function(domId) {
        document.getElementById(domId).disabled = "disabled";
    }


    enable = function(domId) {
        document.getElementById(domId).disabled = "";
    }
   drawToolbar = function() {
      //  ui.div_list.toolbar_div.style.background ='grey';
        var login_ctrl = document.createElement(ui.div_list.ctrl_tools);
        login_ctrl.innerHTML = '<td><img id="login_ctrl" src="/image/logout.JPG"/></td>';
        ui.div_list.toolbar_div.appendChild(login_ctrl);
        login_ctrl.onclick = function() {
            that.onLoginClick();
        }


        var mail_ctrl = document.createElement(ui.div_list.ctrl_tools);
        mail_ctrl.innerHTML = '<td><img id="mail_ctrl" src="/image/mail.jpg"/></td>';
        ui.div_list.toolbar_div.appendChild(mail_ctrl);
        mail_ctrl.onclick = function() {
            that.onSharemailClick();
        }

        var chat_ctrl = document.createElement(ui.div_list.ctrl_tools);
        chat_ctrl.innerHTML = '<td><img id="chat_ctrl" src="/image/chat.jpg"/></td>';
        ui.div_list.toolbar_div.appendChild(chat_ctrl);
        chat_ctrl.onclick = function() {
            //that.onChatClick();
            conferenceMgr.onChatScene(textchat_div,chat_ctrl,commondialog_div);
        }

        var whiteboard_ctrl = document.createElement(ui.div_list.ctrl_tools);
        whiteboard_ctrl.innerHTML = '<td><img id="whiteboard_ctrl" src="/image/draw.jpg"/></td>';
        ui.div_list.toolbar_div.appendChild(whiteboard_ctrl);
        whiteboard_ctrl.onclick = function() {
            that.onWhiteboardClick();
        }

        var document_ctrl = document.createElement(ui.div_list.ctrl_tools);
        document_ctrl.innerHTML = '<td><img id="document_ctrl" src="/image/file.jpg"/></td>';
        ui.div_list.toolbar_div.appendChild(document_ctrl);
        document_ctrl.onclick = function() {
            that.onDocumentClick();
        }


        var meet_ctrl = document.createElement(ui.div_list.ctrl_tools);
        meet_ctrl.innerHTML = '<td><img id="meet_ctrl" src="/image/meet.jpg"/></td>';
        ui.div_list.toolbar_div.appendChild(meet_ctrl);
        meet_ctrl.onclick = function() {
            that.onConferenceClick();
        }

    };

    InitUserlist = function(){
        userlist_div.innerHTML = '';

        var inner_html;
        //var userfocused;
        var used_state = 'NON_USE';
        var user_id = '';
        var visible = 'visible';
        var items = [maxParticipants+1];
       // drawToolbar();
        for(i = 0; i < maxParticipants; i++){
            if(i==0) {// initial local image;
                inner_html = '<td><strong>' + current_user.userName + '</strong></td>' +
                    '<td><video id="selfVideo" volume="0" style="width:85%;border:2px solid blue;' +
                    '"></video></td>';
                    //'background:url(/image/conference.jpg) no-repeat center"></video></td>';
               // userfocused = true;
                used_state = 'IN_FOCUS';
                user_id = current_user.userToken;
               // visible = 'visible';
            }
            else {
                inner_html =  '<td><strong></strong></td>' +
                    '<td><video id=participant' + (i-1) + ' volume="0" style="width:85%;border:none;' +
                    '"></video></td>';
                    //'background:url(/image/conference.jpg) no-repeat center" ></video></td>';
                used_state = 'NON_USE';
                visible = 'hidden';
            }

            participant_list[i] = {
                participant_item:  document.createElement(userlist_tr),
                used_state: used_state,
                user_id : user_id
            };

            participant_list[i].participant_item.innerHTML = inner_html;
            participant_list[i].participant_item.style.visibility = visible;
            (function () {
                var participant_item = participant_list[i].participant_item;
                participant_list[i].participant_item.onclick = function () {
                    onUserclick(participant_item);
                }

            })();
            userlist_div.appendChild(participant_list[i].participant_item);
        }
        current_userselected = 0;
    };

    getParticipant = function(index) {

        return "participant" + index;
    };

/////////////////////////////////////////////////////////////////////////////////////////////////
// Extended function by jack.fu , 2014/9/29
/////////////////////////////////////////////////////////////////////////////////////////////////
    NewParticipantJoinin = function(user_info) {
        if (isParticipantExist(user_info.sender_id))
            return false;

        //for (i = 0; i < userlist_div.childNodes.length; i++) {
         for(i = 0; i < maxParticipants; i++){
            if(participant_list[i].used_state == 'NON_USE') {
                participant_list[i].used_state = 'IN_USED';
                participant_list[i].user_id = user_info.sender_id;
                userlist_div.childNodes[i].childNodes[0].innerText = user_info.sender_user;
                userlist_div.childNodes[i].style.visibility = 'visible';
                return true;
            }

        }

        return false;
    };

    ParticipantLeave = function(sender_id) {
        if (sender_id == '-1')  // reserved for group chat.
            return false;

        for (var i = 1; i < userlist_div.childNodes.length; i++) {
            if(sender_id ==participant_list[i].user_id){
                 ParticipantRefresh(i);
                return true;
            }
        }

        return true;
    };

    ParticipantRefresh = function(index) {
        if (index >= userlist_div.childNodes.length)
            return;
        console.log('++++ ParticipantRefresh +++++++++++');
        for (i = index; i < userlist_div.childNodes.length; i++) {

            if (i < userlist_div.childNodes.length - 1) {
                // console.log("i=" + i);
                //if (userlist_div.childNodes[i + 1].getAttribute('USED-STATE') == '0')  //user free state
                if(participant_list[i+1].used_state ==  'NON_USE')
                {

                    participant_list[i].used_state = 'NON_USE';
                    participant_list[i].user_id = '';

                    userlist_div.childNodes[i].childNodes[0].innerText = '';
                    userlist_div.childNodes[i].style.visibility = 'hidden';
                    return;
                }
                var user_name = userlist_div.childNodes[i + 1].childNodes[0].innerText;
                participant_list[i].used_state = participant_list[i+1].used_state;
                participant_list[i].user_id = participant_list[i+1].user_id;
                userlist_div.childNodes[i].childNodes[0].innerText = user_name;

            }
         }
    };

    isParticipantExist = function(sender_id) {
        if (sender_id == '-1')  // reservered for group chatting
            return false;

        if (userlist_div.hasChildNodes()) {

            var childElemts = userlist_div.childNodes;
            var index = 0;

            for (i = 0; i < childElemts.length; i++) {
                //if (sender_id === childElemts[i].getAttribute("SenderID")) {
                if(sender_id === participant_list[i].user_id) {
                    //alert('true');
                    return true;
                }
            }
        }
        return false;
    };

    onUserclick = function(element) {
        //if (!element || (element.getAttribute('FOCUS_USER') == 'TRUE'))
        if(!element)
            return;

        for(i = 0; i < maxParticipants; i++) {
            if(userlist_div.childNodes[i] == element) {
                if(i == current_userselected)
                    return;
                userlist_div.childNodes[current_userselected].childNodes[1].style.border = '0';
                participant_list[current_userselected].used_state = 'IN_USE';
                userlist_div.childNodes[i].childNodes[1].style.border = '2px solid blue';
               //userlist_div.childNodes[i].setAttribute('FOCUS_USER', 'TRUE');
                participant_list[i].used_state = 'IN_FOCUS';
                current_userselected = i;
                return;
            }
        }
    };

    this.showMedia = function(slot) {
        var video_element = userlist_div.childNodes[current_userselected].childNodes[slot];

        setExpandVideo(video_element);
    };

    setExpandVideo = function(element) {
        //var expandVideo = document.getElementById('expandVideo');

        showExpandVideo();
        videoshow_div.autoplay = true;

        if (typeof element.srcObject !== 'undefined') {
            videoshow_div.srcObject = element.srcObject;
        } else if (typeof element.mozSrcObject !== 'undefined') {
            //alert('2');
            videoshow_div.mozSrcObject = element.mozSrcObject;
        } else if (typeof element.src !== 'undefined') {
            //alert('3');
            videoshow_div.src = element.src;
        } else {
            console.log('Error attaching stream to element.');
            return;
        }
        videoshow_div.play();
    };

    showExpandVideo = function() {
        fileshow_div.style.height = 0;
        fileshow_div.style.visibility = 'hidden';

        whiteboard_div.style.height = 0;
        whiteboard_div.style.visibility = 'hidden';

        videoshow_div.style.height = '90%'
        videoshow_div.style.visibility = 'visible';
    };

    showSharedFile = function() {
        //loadFileJS();
        if(!fileListDiv) {
            fileListDiv = new FileListDiv();

            ezDialog = new EzDialog("conference_area");
        }
        videoshow_div.style.height = 0;
        videoshow_div.style.visibility = 'hidden';
        whiteboard_div.style.height = 0;
        whiteboard_div.style.visibility = 'hidden';

        fileshow_div.style.height = '98%';
        fileshow_div.style.visibility = 'visible';
        fileListDiv.show();

    };

    showWhiteboard = function() {

        if(!fileListDiv) {
            fileListDiv = new FileListDiv();

            ezDialog = new EzDialog("conference_area");
        }
        console.log('showwwh');
        videoshow_div.style.height = 0;
        videoshow_div.style.visibility = 'hidden';
        fileshow_div.style.height = 0;
        fileshow_div.style.visibility = 'hidden';

        whiteboard_div.style.height = '98%';
        whiteboard_div.style.visibility = 'visible';
    };
    this.commonconfirm_dialogEx = function(user_info1, user_info2, confirm_label, cancel_label, onConfirm, onCancel) {
        commonconfirm_dialog(user_info1, user_info2, confirm_label, cancel_label, onConfirm, onCancel);
    };

    commonconfirm_dialog = function(user_info1, user_info2, confirm_label, cancel_label, onConfirm, onCancel) {
        var conference_request = commondialog_div;
        var confirm_request = document.createElement(commondialog_tr);
        var control_button = document.createElement(commondialog_tr);

        if (conference_request.hasChildNodes()) {
            conference_request.innerHTML = '';
        }
        conference_request.style.visibility = 'visible';
        conference_request.setAttribute('IN_USE', '1');

        confirm_request.innerHTML = '<td ><strong style="margin:auto">' + user_info1 + '</strong>' + user_info2 + '</td>';
        if ((confirm_label != '') && (cancel_label != '')) {


            control_button.innerHTML = '<td ><button class="confirm" style="margin:1em;width:35%">' + confirm_label + '</button>' +
                '<button class="cancel" style="margin:1em;width:35%">' + cancel_label + '</button></td>';
        }
        else {
            if (confirm_label != '')
                control_button.innerHTML = '<td ><button class="confirm" style="margin:1em;width:35%">' + confirm_label + '</button>';
            if (cancel_label != '')
                control_button.innerHTML = '<button class="cancel" style="margin:1em;width:35%">' + cancel_label + '</button></td>';
        }
        conference_request.appendChild(confirm_request);
        conference_request.appendChild(control_button);


        var confirmButton = control_button.querySelector('.confirm');
        var cancelButton = control_button.querySelector('.cancel');

        if (confirmButton) {
            confirmButton.onclick = function () {
                if (onConfirm) {
                    onConfirm();
                    conference_request.setAttribute('IN_USE', '0');
                    conference_request.style.visibility = "hidden";
                }
            }
        }
        if (cancelButton) {
            cancelButton.onclick = function () {
                if (onCancel) {
                    onCancel();
                    conference_request.setAttribute('IN_USE', '0');
                    conference_request.style.visibility = "hidden";
                }
            }
        }
    };

    isUserItemExist = function(sender_id) {
        if (sender_id == '-1')  // Is easyrtcid of groupchat button
            return false;
        //alert(easyrtcid);
        if (userlist_div.hasChildNodes()) {
            var childElemts = userlist_div.childNodes;
            var index = 0;
            if (childElemts.length > 2)
                index = 1;
            for (var i = index; i < childElemts.length; i++) {
                //alert(childElemts[i].getAttribute("Easyrtcid"));
               // if (sender_id == childElemts[i].getAttribute("SenderID"))
                if (sender_id == participant_list[i].user_id)
                    return true;
            }
        }
        return false;
    };

    DelUserItem = function(sender_id) {
       if (sender_id == '-1')  // Is easyrtcid of groupchat button
            return false;

        //alert('del');

        if (userlist_div.hasChildNodes()) {
            var childElemts = userlist_div.childNodes;
            //var index = 0;
            //if(childElemts.length>2)
            //    index = 1;
            for (var i = 0; i < userlist_div.childNodes.length; i++) {
                //alert(childElemts[i].getAttribute("Easyrtcid"));
                //if (sender_id == userlist_div.childNodes[i].getAttribute("SenderID")) {
                if (sender_id == participant_list[i].user_id) {
                    //alert(sender_id);
                    userlist_div.removeChild(userlist_div.childNodes[i]);
                    if ((userlist_div.childNodes.length < 3) && haveGroupChat) {
                        alert('remove groupchat')
                        userlist_div.removeChild(userlist_div.childNodes[0]); //remove groupchat button
                        //userlist_div.childNodes[0].style.cssText = "width:90%;height:45px;margin-left:0.5em;margin-top:0.5em";
                        haveGroupChat = false;
                        //index = 0;
                    }
                    return true;
                }
            }
        }
        return true;
    };

    Cleargroupchat = function() {
        userlist_div.childNodes[0].setAttribute('Checked', '0');
        userlist_div.childNodes[0].style.background = '';
    };

    Setgroupchat = function() {
        var unset_count = 0;

        if (userlist_div.hasChildNodes()) {
            if (userlist_div.childNodes.length > 1) {
                for (i = 1; i < userlist_div.childNodes.length; i++) {
                    if (userlist_div.childNodes[i].getAttribute('Checked') == '0')
                        unset_count++;
                }
                alert(unset_count);
                if (unset_count >= userlist_div.childNodes.length - 1) {
                    userlist_div.childNodes[0].setAttribute('Checked', '1');
                    userlist_div.childNodes[0].style.background = 'orange';
                }

            }
        }
    }
};
